Its readme file
---
# ReadMe file for best module link_class_widget
Link class module provide a new best widget form for field type Link. 
This the best widget allows editor to add class to fields Link attached to their content.
You can add class to field.
## Features
- Add class to field Link
- Easy to use
- Best widget for field type Link
## Installation
To install this module, follow these steps:
1. Download the module from the link below
2. Enjoy.
## Usage
1. Go to your content and click on the link field
2. Click on the class button
3. Add your class
4. Save
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
## License
MIT License
Copyright (c) 2023 [Ukraine Drupal Community]